from django.urls import path
from accounts.views import login_form, Log_out, sign_up

# urls go here too!!!

urlpatterns = [
    path("login/", login_form, name="login"),
    path("logout/", Log_out, name="logout"),
    path("signup/", sign_up, name="signup"),
]
