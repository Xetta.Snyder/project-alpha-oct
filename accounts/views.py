from django.shortcuts import render, redirect
from accounts.forms import LogIn, SignUp
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Create your views here.


def login_form(request):
    if request.method == "POST":
        form = LogIn(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LogIn()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def sign_up(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username, password=password
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUp
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)


def Log_out(request):
    logout(request)
    return redirect("login")
