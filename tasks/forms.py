from django import forms
from tasks.models import Task


# forms go here!!!


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "assignee",
            "project",
            "start_date",
            "due_date",
        )
